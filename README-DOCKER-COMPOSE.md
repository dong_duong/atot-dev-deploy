# JHipster generated Docker-Compose configuration

## Usage

Launch all your infrastructure by running: `docker-compose up -d`.

## Configured docker services

### Service registry and configuration server:
- [JHipster Registry](http://localhost:8761)

### Applications and dependencies:
- gateway (gateway application)
- gateway's mariadb database
- gateway's elasticsearch search engine
- webshop (microservice application)
- webshop's mariadb database

### Additional Services:

- [JHipster Console](http://localhost:5601)
Once started, it will be necessary to create the index pattern using the UI and start the `jhipster-import-dashboards` container again with: `docker-compose up jhipster-import-dashboards`
- [Zipkin](http://localhost:9400)
